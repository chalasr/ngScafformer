'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Scafform Schema
 */
var ScafformSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Scafform name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Scafform', ScafformSchema);