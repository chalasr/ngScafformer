'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Scafform = mongoose.model('Scafform'),
	_ = require('lodash');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'Scafform already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a Scafform
 */
exports.create = function(req, res) {
	var scafform = new Scafform(req.body);
	scafform.user = req.user;

	scafform.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(scafform);
		}
		console.log(scafform);
	});
};

/**
 * Show the current Scafform
 */
exports.read = function(req, res) {
	res.jsonp(req.scafform);
};

/**
 * Update a Scafform
 */
exports.update = function(req, res) {
	var scafform = req.scafform;

	scafform = _.extend(scafform, req.body);

	scafform.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(scafform);
		}
	});
};

/**
 * Delete an Scafform
 */
exports.delete = function(req, res) {
	var scafform = req.scafform;

	scafform.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(scafform);
		}
	});
};

/**
 * List of Scafforms
 */
exports.list = function(req, res) {
	Scafform.find().sort('-created').populate('user', 'displayName').exec(function(err, scafforms) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(scafforms);
		}
	});
};

/**
 * Scafform middleware
 */
exports.scafformByID = function(req, res, next, id) {
	Scafform.findById(id).populate('user', 'displayName').exec(function(err, scafform) {
		if (err) return next(err);
		if (!scafform) return next(new Error('Failed to load Scafform ' + id));
		req.scafform = scafform;
		next();
	});
};

/**
 * Scafform authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.scafform.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};
