'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var scafforms = require('../../app/controllers/scafforms');

	// Scafforms Routes
	app.route('/scafforms')
		.get(scafforms.list)
		.post(users.requiresLogin, scafforms.create);
	
	app.route('/scafforms/:scafformId')
		.get(scafforms.read)
		.put(users.requiresLogin, scafforms.hasAuthorization, scafforms.update)
	    .delete(users.requiresLogin, scafforms.hasAuthorization, scafforms.delete);

	// Finish by binding the Scafform middleware
	app.param('scafformId', scafforms.scafformByID);
};