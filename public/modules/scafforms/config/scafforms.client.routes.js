'use strict';

//Setting up route
angular.module('scafforms').config(['$stateProvider',
	function($stateProvider) {
		// Scafforms state routing
		$stateProvider.
		state('listScafforms', {
			url: '/scafforms',
			templateUrl: 'modules/scafforms/views/list-scafforms.client.view.html'
		}).
		state('createScafform', {
			url: '/scafforms/create',
			templateUrl: 'modules/scafforms/views/create-scafform.client.view.html'
		}).
		state('viewScafform', {
			url: '/scafforms/:scafformId',
			templateUrl: 'modules/scafforms/views/view-scafform.client.view.html'
		}).
		state('editScafform', {
			url: '/scafforms/:scafformId/edit',
			templateUrl: 'modules/scafforms/views/edit-scafform.client.view.html'
		});
	}
]);