'use strict';

// Configuring the Articles module
angular.module('scafforms').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Scafforms', 'scafforms');
		Menus.addMenuItem('topbar', 'New Scafform', 'scafforms/create');
	}
]);