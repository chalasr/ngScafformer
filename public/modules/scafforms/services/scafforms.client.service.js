'use strict';

//Scafforms service used to communicate Scafforms REST endpoints
angular.module('scafforms').factory('Scafforms', ['$resource', function($resource) {
    return $resource('scafforms/:scafformId', {
        scafformId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);