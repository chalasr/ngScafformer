'use strict';

// Scafforms controller
angular.module('scafforms').controller('ScafformsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Scafforms',
    function($scope, $stateParams, $location, Authentication, Scafforms) {

        var data;
        var count = 0;
        $scope.authentication = Authentication;
        $scope.formData = {};
        $scope.opt = {};
        $scope.schema = [];

        $scope.addTextField = function(){
          document.getElementById('newtextfield').style.display = 'block';
        };

        $scope.addCheckField = function(){
          document.getElementById('newcheckfield').style.display = 'block';
        };

        $scope.addSelectField = function(){
          document.getElementById('newselectfield').style.display = 'block';
        };


        $scope.submitTextField = function(data){
          data = $scope.formData;
          console.log(data);
          $scope.schema.push({property: 'property', label: data.label, type: data.type, attr:{class : 'form-control'} });
          $scope.formData = {};
          document.getElementById('newtextfield').style.display = 'none';
        };

        $scope.submitCheckField = function(data){
          data = $scope.formData;
          console.log(data);
          $scope.schema.push(
            {property: 'checklabel', label: data.label, type: 'text' },
            {property: 'property1', label: data.value1, type: 'checkbox', attr: {class: 'form-group', value: data.value1 } }
          );
          $scope.schema.push(
            {property: 'property2', label: data.value2, type: 'checkbox', attr: {class: 'form-group', value: data.value2 } }
          );
          $scope.formData = {};
          setTimeout("document.getElementById('checklabel').style.display = 'none';",500 );
          document.getElementById('newcheckfield').style.display = 'none';
        };

        $scope.submitSelectField = function(data){
          data = $scope.formData;
          console.log(data);
            $scope.opt = {
              0: data.value1,
              1: data.value2,
            };
          $scope.schema.push(
            {property: 'select', label: data.label, type: 'select', list: 'key as value for (key,value) in opt', attr:{class : 'form-control' } }
          );
          $scope.formData = {};
          document.getElementById('newselectfield').style.display = 'none';
        };

        $scope.addSelectOption = function(data){
          var input = document.createElement('input');
          input.type = 'text';
          input.name = 'value3';
          input.id = 'choix3';
          document.getElementById('selectlist').appendChild(input);
          var field = document.getElementById('choix3');
          var att = document.createAttribute('ng-model');
          att.value = 'formData.value3';
          field.setAttributeNode(att);
        };

        $scope.logForm = function(){
          console.log($scope.schema);
        };

         //Create new Scafform
        $scope.create = function() {
        	// Create new Scafform object
            console.log(this);
            var scafform = new Scafforms({
                name: this.name,
                toto: this.toto
            });
            // Redirect after save
            scafform.$save(function(response) {
                $location.path('scafforms/' + response._id);
            }, function(errorResponse) {
      				$scope.error = errorResponse.data.message;
      			});
            // Clear form fields
            this.name = '';
        };

        // Remove existing Scafform
        $scope.remove = function(scafform) {
            if (scafform) {
                scafform.$remove();

                for (var i in $scope.scafforms) {
                    if ($scope.scafforms[i] === scafform) {
                        $scope.scafforms.splice(i, 1);
                    }
                }
            } else {
                $scope.scafform.$remove(function() {
                    $location.path('scafforms');
                });
            }
        };

        // Update existing Scafform
        $scope.update = function() {
            var scafform = $scope.scafform;

            scafform.$update(function() {
                $location.path('scafforms/' + scafform._id);
            }, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
        };

        // Find a list of Scafforms
        $scope.find = function() {
            $scope.scafforms = Scafforms.query();
        };

        // Find existing Scafform
        $scope.findOne = function() {
            $scope.scafform = Scafforms.get({
                scafformId: $stateParams.scafformId
            });
        };
    }
]);
