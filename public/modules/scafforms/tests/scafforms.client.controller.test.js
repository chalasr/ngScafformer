'use strict';

(function() {
	// Scafforms Controller Spec
	describe('Scafforms Controller Tests', function() {
		// Initialize global variables
		var ScafformsController,
			scope,
			$httpBackend,
			$stateParams,
			$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Scafforms controller.
			ScafformsController = $controller('ScafformsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Scafform object fetched from XHR', inject(function(Scafforms) {
			// Create sample Scafform using the Scafforms service
			var sampleScafform = new Scafforms({
				name: 'New Scafform'
			});

			// Create a sample Scafforms array that includes the new Scafform
			var sampleScafforms = [sampleScafform];

			// Set GET response
			$httpBackend.expectGET('scafforms').respond(sampleScafforms);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.scafforms).toEqualData(sampleScafforms);
		}));

		it('$scope.findOne() should create an array with one Scafform object fetched from XHR using a scafformId URL parameter', inject(function(Scafforms) {
			// Define a sample Scafform object
			var sampleScafform = new Scafforms({
				name: 'New Scafform'
			});

			// Set the URL parameter
			$stateParams.scafformId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/scafforms\/([0-9a-fA-F]{24})$/).respond(sampleScafform);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.scafform).toEqualData(sampleScafform);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Scafforms) {
			// Create a sample Scafform object
			var sampleScafformPostData = new Scafforms({
				name: 'New Scafform'
			});

			// Create a sample Scafform response
			var sampleScafformResponse = new Scafforms({
				_id: '525cf20451979dea2c000001',
				name: 'New Scafform'
			});

			// Fixture mock form input values
			scope.name = 'New Scafform';

			// Set POST response
			$httpBackend.expectPOST('scafforms', sampleScafformPostData).respond(sampleScafformResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Scafform was created
			expect($location.path()).toBe('/scafforms/' + sampleScafformResponse._id);
		}));

		it('$scope.update() should update a valid Scafform', inject(function(Scafforms) {
			// Define a sample Scafform put data
			var sampleScafformPutData = new Scafforms({
				_id: '525cf20451979dea2c000001',
				name: 'New Scafform'
			});

			// Mock Scafform in scope
			scope.scafform = sampleScafformPutData;

			// Set PUT response
			$httpBackend.expectPUT(/scafforms\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/scafforms/' + sampleScafformPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid scafformId and remove the Scafform from the scope', inject(function(Scafforms) {
			// Create new Scafform object
			var sampleScafform = new Scafforms({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Scafforms array and include the Scafform
			scope.scafforms = [sampleScafform];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/scafforms\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleScafform);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.scafforms.length).toBe(0);
		}));
	});
}());