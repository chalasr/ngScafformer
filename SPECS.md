Paramètres Généraux :
 - Afficher la barre de progression en bas des pages de formulaire (optionnel)
 - N'autoriser qu'une seule réponse par personne (connexion requise) (optionnel)
 - Trier les questions en mode aléatoire (optionnel)
 - Titre et description du formulaire

Paramètres des questions et réponses possibles :
 - Intitulé de la question
 - Texte de l’aide
 - Type de question :
 - Simples
   .Texte
   .Texte de paragraphe
   .Choix multiples
   .Cases à cocher
   .Sélectionner dans une liste
 - Complexes :
   .Echelle d’évaluation (optionnel)
   .Grille (optionnel)
   .Date
   .Heure
 - Ouvrir une page en fonction de la réponse
 - Options (radio, checkbox, input, etc… autre).
   .Trier les options en mode aléatoire
 - Question obligatoire ou optionnelle

 Paramètres de mise en page :
  - Entête de section (groupement de questions)
    .Titre de la section
    .Description de la section (facultative)
  - Saut de page
  - Image
    .Depuis une URL uniquement
  - Vidéo
    .Depuis une URL uniquement
 Paramètres de confirmation :
  - Afficher le lien pour envoyer une autre réponse (réafficher le formulaire une nouvelle fois).
  - Publier et afficher un lien public vers les résultats de ce formulaire.
  - Permettre aux personnes interrogées de modifier leurs réponses après l’envoi du formulaire.
 Choisir l’action après chaque page, passer à la suivante, une autre ou envoyer le formulaire ?
 Page « Remplir le Formulaire »
 - Pouvoir répondre aux différentes questions comme « Google Forms » et enregistrer les
 résultats.
 Page « Afficher les résultats »
 - Afficher les réponses dans un tableau HTML et exporter le tableau dans un CSV
 Bonus
 - Validation des données (règles de validation du formulaire, regexp, contrôle des champs)
 - Panneau d’administration permettant de gérer ses formulaires et résultats (compte requis).
